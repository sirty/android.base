package vn.vti.common.android.mvp;

/**
 * Created by VTI Android Team on 11/10/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
public interface BaseListContract {

    interface View extends BaseContract.View {

        void showLoading();

        void hideLoading();

        void showError(String errorMessage);
    }

    interface Presenter extends BaseContract.Presenter<View> {

    }

}
