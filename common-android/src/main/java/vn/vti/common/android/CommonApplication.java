package vn.vti.common.android;

import android.app.Application;

import timber.log.Timber;
import vn.vti.common.android.util.timber.CrashReportTree;
import vn.vti.common.android.util.timber.DebugReportTree;

/**
 * Created by VTI Android Team on 11/13/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
public class CommonApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new DebugReportTree());
        } else {
            Timber.plant(new CrashReportTree());
        }
    }

}
