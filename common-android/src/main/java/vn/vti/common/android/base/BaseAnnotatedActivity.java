package vn.vti.common.android.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

import vn.vti.common.android.mvp.BaseContract;
import vn.vti.common.android.util.AnnotationHelper;
import vn.vti.common.android.util.Viewable;

/**
 * Created by VTI Android Team on 11/10/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
public abstract class BaseAnnotatedActivity<V extends BaseContract.View, P extends BaseContract.Presenter<V>> extends BaseActivity<V, P> {

    @SuppressWarnings("unchecked")
    @Override
    protected P initPresenter() {
        return (P) AnnotationHelper.createPresenter(getClass());
    }

    @Override
    protected int getResourceLayout() {
        int layoutResId = getClass().getAnnotation(Viewable.class).layout();
        if (layoutResId != Viewable.LAYOUT_NOT_DEFINED) {
            return layoutResId;
        }
        return 0;
    }
}
