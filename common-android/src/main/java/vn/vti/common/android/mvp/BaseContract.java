package vn.vti.common.android.mvp;

import android.arch.lifecycle.Lifecycle;
import android.os.Bundle;

/**
 * Created by VTI Android Team on 11/10/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
public interface BaseContract {

    interface View {

    }

    interface Presenter<V extends BaseContract.View> {

        Bundle getStateBundle();

        void attachLifecycle(Lifecycle lifecycle);

        void detachLifecycle(Lifecycle lifecycle);

        void attachView(V view);

        void detachView();

        V getView();

        boolean isViewAttached();

        void onPresenterCreated();

        void onPresenterDestroy();
    }

}
