package vn.vti.common.android.listener;

import android.view.View;

/**
 * Created by VTI Android Team on 11/10/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */

public interface OnRecyclerViewItemClick<T> {

    void onItemClick(View view, T item);

}
