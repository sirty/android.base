package vn.vti.common.android.util.timber;

import android.util.Log;

import timber.log.Timber;

/**
 * Created by VTI Android Team on 11/13/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
@SuppressWarnings("WeakerAccess")
public class CrashReportTree extends Timber.Tree {

    @SuppressWarnings("UnnecessaryReturnStatement")
    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
            return;
        }

        //TODO: Do some works here
    }

}
