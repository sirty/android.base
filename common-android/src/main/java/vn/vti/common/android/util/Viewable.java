package vn.vti.common.android.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import vn.vti.common.android.mvp.BasePresenter;

/**
 * Created by VTI Android Team on 11/10/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Viewable {
    int LAYOUT_NOT_DEFINED = -1;

    Class<? extends BasePresenter> presenter();

    int layout() default LAYOUT_NOT_DEFINED;
}
