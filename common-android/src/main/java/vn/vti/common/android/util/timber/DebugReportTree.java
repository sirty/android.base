package vn.vti.common.android.util.timber;

import timber.log.Timber;

/**
 * Created by VTI Android Team on 11/13/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
public class DebugReportTree extends Timber.DebugTree {

    @Override
    protected String createStackElementTag(StackTraceElement element) {
        return String.format("[C:%s] [M:%s] [L:%s]", super.createStackElementTag(element),
                element.getMethodName(), element.getLineNumber());

    }

}
