package vn.vti.common.android.mvp;

/**
 * Created by VTI Android Team on 11/10/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
public class MvpException extends RuntimeException {

    public MvpException(String message) {
        super(message);
    }

    public MvpException(String message, Throwable cause) {
        super(message, cause);
    }

    public MvpException(Throwable cause) {
        super(cause);
    }
}
