package vn.vti.common.kotlin.mvp

/**
 * Created by VTI Android Team on 11/13/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
interface BaseListContract {

    interface View : BaseContract.View {

        fun showLoading()

        fun hideLoading()

        fun showError(errorMessage: String)
    }

    interface Presenter : BaseContract.Presenter<View>

}
