package vn.vti.common.kotlin.mvp

import android.arch.lifecycle.Lifecycle
import android.os.Bundle

/**
 * Created by VTI Android Team on 11/13/2017.
 * Copyright © 2017 VTI Inc. All rights reserved.
 */
interface BaseContract {

    interface View

    interface Presenter<V : BaseContract.View> {

        val stateBundle: Bundle

        val view: V

        val isViewAttached: Boolean

        fun attachLifecycle(lifecycle: Lifecycle)

        fun detachLifecycle(lifecycle: Lifecycle)

        fun attachView(view: V)

        fun detachView()

        fun onPresenterCreated()

        fun onPresenterDestroy()
    }

}
